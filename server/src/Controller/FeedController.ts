

import Feed from '../Model/Feed'
import { Request, Response } from 'express'


class FeedController {

    public async index(req: Request, res: Response){
        const feeds = await Feed.find()
        return res.json(feeds)
    }

    public async show(req: Request, res: Response): Promise<Response> {
        const feeds = await Feed.findById(req.params.id)
        return res.json(feeds)
    }

    public async create(req: Request, res: Response): Promise<Response> {
        const feeds = await Feed.create(req.body)
        return res.json(feeds)
    }
    public async destroy(req: Request, res: Response): Promise<Response> {
        const feeds = await Feed.findByIdAndDelete(req.params.id)
        return res.json(feeds)
    }
}

export default new FeedController()



