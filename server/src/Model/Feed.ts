import{ Schema, model, Document } from 'mongoose';

interface FeedInterface extends Document {

    titulo?: string;
    descricao?: string
    url?: string
}

const FeedInsta = new Schema({

    titulo: String,
    descricao: String,
    url: String,
})


export default model<FeedInterface>('Feed', FeedInsta)

