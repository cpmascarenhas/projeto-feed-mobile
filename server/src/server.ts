import ConnectDB from './Database/ConnectDB'


if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

ConnectDB.express.listen(process.env.PORT || 3003, () => {
    console.log("rodando na porta 3003")
})