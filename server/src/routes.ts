import {Router} from 'express';
import FeedController from './Controller/FeedController';

const routes = Router()

routes.get('/feed', FeedController.index)
routes.get('/feed/:id', FeedController.show)
routes.post('/feed', FeedController.create)
routes.delete('/feed/:id', FeedController.destroy)

export default routes;
