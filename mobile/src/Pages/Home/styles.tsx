import React from 'react';
import { StyleSheet } from 'react-native';


const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',

    },

    text: {
        marginTop: 25,
        fontWeight: 'bold',
        fontSize: 20
    },

})

export default Styles;
