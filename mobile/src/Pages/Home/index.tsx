import React from 'react';
import { View, Text, Image, StyleSheet, ScrollView } from 'react-native';
import Fotos from '../../Components/Fotos';
import Styles from './styles';

const Home = () => {
    return (
        <ScrollView>
            <>
                <View style={Styles.container}>
                    <Text style={Styles.text}>Viagem de carro</Text>
                    <Fotos />
                </View>
            </>
        </ScrollView>

    );
};
export default Home;


