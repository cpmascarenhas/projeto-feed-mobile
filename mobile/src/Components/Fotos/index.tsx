import React from 'react';
import { Image, StyleSheet} from 'react-native';

const Fotos = () => {
    return (
        <>
            <Image
                style={styles.image}
                source={require("../../Assets/viagem.jpg")} />
        </>
    );
};

const styles = StyleSheet.create({

    image: {
        marginTop: 20,
        borderRadius: 10,
        width: 360,
        height: 400,
        resizeMode: 'stretch',
    }
})

export default Fotos;


