import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = () => {
    return (
        <>
            <View style={styles.container}>
                <Text style={styles.text}>Feed Notícia</Text>
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        width: '100%',
        height: '10%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#836FFF',
    },
    text: {
        color: '#ffff',
        fontWeight: 'bold',
        fontSize: 20
    }
})

export default Header;


