import React from 'react';
import {View, Text } from 'react-native';
import Header from './Components/Header';
import Home from './Pages/Home';

export default function App () {
  return (
    <>
      <View>
        <Header />
        <Home />
      </View>
    </>
  );
};


